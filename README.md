Prerequisite: 

	- route 53'den 2 tane hostname al.
	- kubernetes ec2 ip adresini subdomainname.hostname (frontend.tugbakorkut.link)(frontendtest.tugbakorkut.link) ismini ata.
	
1. AWS cloudformation template kullanılarak kubespray ile tek node lu bir k8s cluster kuruldu. 

2. K8s cluster da Helm ile ingress controller deploy edildi.

3. Sertifika için cert manager kuruldu.

4. Gitlab de production environmentı için main, test environmentı için test branch'ları oluşturuldu.

5. Docker image larının build ve push işlemelerinin yapılacağı gitlab runner olarak çalışması için 1 adet ubuntu instance oluşturulup docker ve gitlab runner kuruldu.

6. Üç aşamalı pipeline oluşturuldu.
	- Birinci aşamada frontend ve backend imaajları build alındı.
	- ikinci aşamada build alınan bu imajlar DockerHub daki repostorye push edildi.
	- üçüncü aşamada Helm ile deployment işlemi yapıldı.

7. Helm'de tek values file ile  Frontend ve Backend ve database template hazırlandı.

8. Helm ile ilgili dosyalar repostory de Helm klasörünün içinde bulunmaktadır.

9. CI/CD Pİpeline için  .GitLap-ci.yml oluşturuldu.

10. Postgre için PV, PVC oluşturuldu.

11. Projenin bulunduğu GitLab adresi https://gitlab.com/arslantobe/codeway_task.git



